package com.xbc.ibds.core.status.enums;

import com.xbc.ibds.core.status.StatusCode;

/**
 * 
 * <p>通用状态码枚举类</p>
 * <ul>
 * <li><b>Copyright</b> 浙江核新同花顺网络信息股份有限公司iFinD</li>
 * <li><b>Date</b> 2019年1月30日 </li>
 * </ul>
 * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
 */
public enum StatusEnum implements StatusCode {
	OK(0, "success"),
	ERROR(-1, "error"),
	SYS_ERROR(-2, "系统内部错误"),
	UNKNOWN_ERROR(-3, "未知错误"),
	SERVICE_DOWN(-4, "服务被关闭"),
	SERVICE_OVERLOAD(-5, "服务过载"),
	CALL_EXTERNAL_SERVICE_FAILED(-6, "依赖外部服务请求失败"),
	TIMEOUT(-7, "服务超时"),
	WRONG_URL(-8, "接口地址错误"),
	WRONG_AUTH_TYPE(-100, "认证方式有误"),
	APPID_MISSING(-101, "appid或secret为空"),
	APPID_NOT_EXIST(-102,"appid不存在"),
	WRONG_SECRET(-103,"secret错误"),
	TOKEN_DISABLED(-104,"token不可用(应用账号异常)"),
	CREATE_TOKEN_FAILED(-105,"token生成失败"),
	TOKEN_INVALID(-106,"token无效(token参数错误或格式错误)"),
	IP_FORBIDDEN(-107,"禁止ip访问"),
	SERVICE_ACCESS_FORBIDDDEN(-108,"没有该接口访问权限"),
	GRANT_TYPE_UNSUPPORTED(-109,"不支持grant_type的类型"),
	OVER_AUTH_REQUEST_LIMIT(-110,"超过授权请求限制"),
	OVER_REQUEST_LIMIT(-111,"超过请求次数限制"),
	ACCOUNT_EXCEPTION(-112,"应用账号异常"),
	QPS_OVER_LIMIT(-113,"qps超过阈值"),
	QPS_ERROR(-114,"qps限流异常"),
	OVER_TOTAL_REQUEST_LIMIT(-115,"请求总量限制"),
	AUTHORIZATION_PARAMETER_ERROR(-201,"授权参数错误"),
	LIMITING_RESET_ERROR(-202,"限流重置错误"),
	INTERFACE_NON_EXIST(-203,"接口不存在"),
	REQUEST_PARAMETER_EMPTY(-300,"请求参数为空"),
	LENGTH_OVER_LIMIT(-301,"请求长度超过限制"),
	LACK_OF_NECESSARY_PARAMETERS(-302,"缺少必要参数（%s）"), 
	PARAMETER_VALUE_ILLEGAL(-303,"参数值非法, 需为 (%s)，实际为 (%s)"),
	PARAMETER_LENGTH_NON_REQUIREMENTS(-304,"参数(%s)长度不符合要求（过长或过短）"),
	PARAMETER_FORMAT_ERROR(-305,"参数(%s)格式错误"),
	ERROR_NON_RETURNED(-306,"错误，无数据返回"),
	DATA_UNAUTHORIZED_ACCESS(-307,"数据无权限访问"),
	JS_SDK_AUTHENTICATION_FAILURE(-401,"js-sdk鉴权失败"),
	JS_SDK_REQUEST_ERROR(-402,"js-sdk请求错误"),
	MOCK_PARAMETER_FORMAT_ERROR(-500,"mock请求参数格式错误"),
	MOCK_NON_NECESSARY_PARAMETERS(-501,"mock缺少必要参数"),
	MOCK_INTERFACE_NON_EXIST(-502,"mock接口不存在"),
	MOCK_NON_DATA(-503,"mock无数据"),
	NO_ANSWER_NO_RESULT(300,"提示:无答案,无结果"),
	PARTIAL_NO_RESULT(301,"提示:部分无结果"),
	// 未获取到分布式锁
    LOCKED(-120,"系统繁忙，请稍后再试！"),
    MISSING_PARAM(-302, "缺少必要参数"),   
    INVALID_PARAM(-303, "参数值非法"),
    HTTP_URL_NOT_FUND(-404, "URL不存在"),
    HTTP_CONNECT_EXCEPTION(-406, "HTTP连接错误"),
    HTTP_REQUEST_TIMEOUT(-408, "HTTP请求超时"),   
    REMOTE_RESULT_FAILED(-601, "远程接口返回失败信息"),
    REMOTE_RESULT_NULL(-602, "远程接口返回数据为空"),
	NO_PERMISSION(-5004,"没有权限！"),

	FLOW_EXISTED(-11201,"已存在审核中的流程，禁止重复提交"),
    FLOW_NOT_EXISTED(-11202,"不存在审核中的流程"),
	FLOW_ERROR_STATUS(-11203,"流程状态错误"),
	FLOW_CONFIG_NOT_EXIST(-11204,"请联系系统管理员配置审核流程"),
    FLOW_OPERATION_FORBIDDEN(-11205,"该有该流程的操作权限")
    ;
    
    
    
	StatusEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * 错误代码
	 */
	private int code;

	/**
	 * 错误描述
	 */
	private String message;

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	/**
	 * 输出格式：code|message
	 */
	@Override
	public String toString() {
		return this.code + "|" + this.message;
	}

}
