package com.xbc.ibds.core.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringUtils extends org.apache.commons.lang3.StringUtils {
    public static final String CHARSET_ENCODING = "UTF-8";

    public static final String FILE_SEPARATOR = File.separator;

    public static boolean isEmpty(String str) {
        return null == str || "".equals(str);
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static String getStackTraceAsString(Exception e) {
        StringWriter stringWriter = null;
        PrintWriter printWriter = null;
        StringBuffer error = null;
        try {
            stringWriter = new StringWriter();
            printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            error = stringWriter.getBuffer();
            return error.toString();
        } catch (Throwable localThrowable) {
            try {
                if (printWriter != null) {
                    printWriter.flush();
                    printWriter.close();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            try {
                if (stringWriter != null) {
                    stringWriter.flush();
                    stringWriter.close();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } finally {
            try {
                if (printWriter != null) {
                    printWriter.flush();
                    printWriter.close();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            try {
                if (stringWriter != null) {
                    stringWriter.flush();
                    stringWriter.close();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return "";
    }

    /**
     * 功能描述:转字符串为Long类型List
     *
     * @param str
     * @param split
     * @return:
     * @author: 薛行晨(RoyalXC)
     * @date: 2019/1/15 16:24
     */
    public static List<Long> convertToList(String str, String split) {
        return Arrays.stream(str.split(split)).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
    }

    public static String joinFilePath(String... dirs) {
        return StringUtils.join(dirs, FILE_SEPARATOR);
    }

    public static void main(String[] args) {
        System.out.println(joinFilePath("asd", "qwe"));
    }
}
