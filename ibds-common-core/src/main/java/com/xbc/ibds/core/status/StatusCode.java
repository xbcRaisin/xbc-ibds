package com.xbc.ibds.core.status;

/**
 * 
 * <p>状态码接口</p>
 * <ul>
 * <li><b>Copyright</b> 浙江核新同花顺网络信息股份有限公司iFinD</li>
 * <li><b>Date</b> 2019年1月31日 </li>
 * </ul>
 * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
 */
public interface StatusCode {
    
    /**
     * 获取状态码
     * <p>说明：code小于0表示错误/0表示正常/大于0表示正常的提示；数据范围-9999~9999</p>
     * <p>-999~999为系统保留的通用状态码，超出该范围错误码为各业务的自定义错误码</p>
     * <p>自定义状态码规范：两位业务领域编码（10~99）+两位具体状态码</p>
     * @return
     * @Date 2019年1月31日
     * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
     */
    int getCode();
    
    /**
     * 获取提示信息
     * @return
     * @Date 2019年1月31日
     * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
     */
    String getMessage();
}