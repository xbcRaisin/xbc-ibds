package com.xbc.ibds.core.exception;


import com.xbc.ibds.core.status.StatusCode;

/**
 * 
 * <p>
 * 数据校验失败异常类
 * </p>
 * <ul>
 * <li><b>Copyright</b> 浙江核新同花顺网络信息股份有限公司iFinD</li>
 * <li><b>Date</b> 2019年1月31日</li>
 * </ul>
 * 
 * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
 */
public class DataValidateException extends BizException {

    private static final long serialVersionUID = 613683318965988216L;

    public DataValidateException(StatusCode statusCode) {
        super(statusCode);
    }

    public DataValidateException(StatusCode statusCode, String message) {
        super(statusCode, message);
    }

    public DataValidateException(StatusCode statusCode, String message, String detailMessge) {
        super(statusCode, message, detailMessge);
    }
}
