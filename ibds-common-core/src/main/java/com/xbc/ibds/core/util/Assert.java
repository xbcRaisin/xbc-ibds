package com.xbc.ibds.core.util;

import com.xbc.ibds.core.exception.DataValidateException;
import com.xbc.ibds.core.status.StatusCode;


/**
 * 功能描述: 校验工具类
 *
 * @author: xuexingchen
 * @date: 2018/9/28 15:47
 */
public class Assert {
    private static void validateFailed(StatusCode codeEnum, String message) {
        if (StringUtils.isEmpty(message)) {
            throw new DataValidateException(codeEnum);
        }
        throw new DataValidateException(codeEnum, message);
    }

    public static void isTrue(boolean value, StatusCode codeEnum, String message) {
        if (!value) {
            Assert.validateFailed(codeEnum, message);
        }
    }

    public static void isTrue(boolean value, StatusCode codeEnum) {
        Assert.isTrue(value, codeEnum, null);
    }

    public static void isNull(Object object, StatusCode codeEnum) {
        Assert.isNull(object, codeEnum, null);
    }

    private static void isNull(Object object, StatusCode codeEnum, String message) {
        if (object != null) {
            Assert.validateFailed(codeEnum, message);
        }
    }

    public static void notNull(Object object, StatusCode codeEnum, String message) {
        if (object == null) {
            Assert.validateFailed(codeEnum, message);
        }
    }

    public static void notNull(Object object, StatusCode codeEnum) {
        Assert.notNull(object, codeEnum, null);
    }

    public static void notEmpty(String value, StatusCode codeEnum, String message) {
        if (value == null || value.length() == 0) {
            Assert.validateFailed(codeEnum, message);
        }
    }

    public static void notEmpty(String value, StatusCode codeEnum) {
        Assert.notEmpty(value, codeEnum, null);
    }

}
