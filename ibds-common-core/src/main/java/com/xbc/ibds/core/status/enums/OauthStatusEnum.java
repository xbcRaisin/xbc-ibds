package com.xbc.ibds.core.status.enums;


import com.xbc.ibds.core.status.StatusCode;

/**
 * 描述：oauth服务错误码
 *
 * @author xuexingchen@myhexin.com
 * @date 2019/8/26 13:40
 */
public enum OauthStatusEnum implements StatusCode {
    JWT_TIME_EXPIRED(10100, "令牌已过期！"),
    JWT_VERIFY_SIGNATURE_ERROR(10101, "令牌密钥校验失败！"),
    JWT_VERIFY_IP_ERROR(10102, "令牌IP校验失败！"),
    UNKNOWN_JWT_VERIFY_ERROR(10103, "令牌校验未知错误！"),
    JWT_PWD_VERIFY_ERROR(10104, "密码错误，请重新登录！"),
    AUTH_PERMISSION_DENIED_ERROR(10105, "无权限访问，请联系管理员！"),
    USER_NOT_FOUND(10106, "无此用户！"),
    USER_PWD_NOT_MATCH(10107, "密码错误，请重新登录！"),
    USER_STATE_NOT_ACTIVE(10108, "该用户已被禁用，请联系管理员！"),
    USER_NOT_LOGIN(-5003, "请登录！")
    ;
    private int code;
    private String message;

    private OauthStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.code + "|" + this.message;
    }
}
