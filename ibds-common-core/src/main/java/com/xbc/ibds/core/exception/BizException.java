package com.xbc.ibds.core.exception;

import com.xbc.ibds.core.status.StatusCode;
import com.xbc.ibds.core.status.enums.StatusEnum;

/**
 * 
 * <p>统一业务异常类</p>
 * <p>系统中所有业务的自定义异常均应继承于此类</p>
 * <ul>
 * <li><b>Copyright</b> 浙江核新同花顺网络信息股份有限公司iFinD</li>
 * <li><b>Date</b> 2019年1月31日</li>
 * </ul>
 * 
 * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
 */
public class BizException extends RuntimeException {

    private static final long serialVersionUID = 1162363440700636313L;

    /**
     * 预定义的状态码枚举对象
     */
    protected StatusCode statusCode;

    /**
     * 异常提示信息，一般用于返回到ApiResult.message字段
     */
//    protected String message;

    /**
     * 业务异常详细信息
     */
    protected String detailMessge;

    // --------------构造方法--------------------

    /**
     * Constructs
     * 
     * @param statusCode 状态码枚举对象
     */
    public BizException(StatusCode statusCode) {
        super(statusCode.getMessage());
        this.statusCode = statusCode;
    }

    /**
     * Constructs
     * 
     * @param statusCode 状态码枚举对象
     * @param message    自定义提示信息
     */
    public BizException(StatusCode statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
    }

    /**
     * Constructs
     * 
     * @param statusCode 状态码枚举对象
     * @param message    自定义提示信息
     * @param innerMsg   业务异常详细信息
     */
    public BizException(StatusCode statusCode, String message, String detailMessge) {
        this(statusCode, message);
        this.detailMessge = detailMessge;
    }

    /**
     * Constructs
     * 
     * @param cause      the cause
     * @param statusCode 状态码枚举对象
     */
    public BizException(Throwable cause, StatusCode statusCode) {
        super(statusCode.getMessage(), cause);
        this.statusCode = statusCode;
    }

    /**
     * Constructs
     * 
     * @param cause      the cause
     * @param statusCode 状态码枚举对象
     * @param message    自定义提示信息
     */
    public BizException(Throwable cause, StatusCode statusCode, String message) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    /**
     * Constructs
     * 
     * @param cause      the cause
     * @param statusCode 状态码枚举对象
     * @param message    自定义提示信息
     * @param innerMsg   业务异常详细信息
     */
    public BizException(Throwable cause, StatusCode statusCode, String message, String detailMessge) {
        this(cause, statusCode, message);
        this.detailMessge = detailMessge;
    }

    // --------------getter and setter--------------------

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }
    
    public int getCode() {
        return getStatusCode().getCode();
    }

    /**
     * 获取自定义信息
     */
    @Override
    public String getMessage() {
        if (super.getMessage() != null && !"".equals(super.getMessage())) {
            return super.getMessage();
        }
        return statusCode.getMessage();
    }

    /**
     * 获取详细信息
     * 
     * @return
     * @Date 2019年1月31日
     * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
     */
    public String getDetailMessage() {
        return this.detailMessge;
    }
    
    /**
     * 设置详细信息
     * @param detailMessage
     * @Date 2019年2月13日
     * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
     */
    public void setDetailMessage(String detailMessage) {
        this.detailMessge = detailMessage;
    }
    
    public String toSimpleString() {
        return "Code:" + getCode() + ",message:" + this.getMessage() + "detailMsg:" + getDetailMessage();
    }

    public static void main(String[] args) {
        BizException e = new BizException(StatusEnum.ERROR, "发生业务异常", "这里是异常详细信息");
        System.out.println(e.getDetailMessage());
        System.out.println(e.getMessage());
        e.printStackTrace();
    }
}
