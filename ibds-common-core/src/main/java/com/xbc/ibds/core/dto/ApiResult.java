package com.xbc.ibds.core.dto;

import com.xbc.ibds.core.status.StatusCode;
import com.xbc.ibds.core.status.enums.StatusEnum;

import java.io.Serializable;


/**
 * 
 * <p>
 * Api标准返回结果包装类
 * </p>
 * <p>如果无需返回data数据，则泛型T可声明为EmptyData类型</p>
 * <ul>
 * <li><b>Copyright</b> 浙江核新同花顺网络信息股份有限公司iFinD</li>
 * <li><b>Date</b> 2019年1月30日</li>
 * </ul>
 * 
 * @author <a href="mailto:zhouwenchao@myhexin.com">ZhouWenchao</a>
 * @param <T> data的数据类型
 */
public class ApiResult<T> implements Serializable {
    private static final long serialVersionUID = -6799598102436959543L;

    /**
     * 状态码
     * 小于0表示错误/0表示正常/大于0表示正常的提示
     */
    private Integer status;

    /**
     * 信息提示，可直接用于前端展示给用户。
     */
    private String message;

    /**
     * 接口返回的业务数据，格式自由
     */
    private T data;

    /**
     * 内部错误信息，比如堆栈信息，用于调试程序排查错误，一般不直接输出给用户。
     */
    private String innerMsg;

    // -----------无返回数据构造方法------------

    private ApiResult() {
        this(StatusEnum.OK);
    }

    /**
     * 根据状态码构建默认ApiResult对象
     * 
     * @param statusCode 状态码枚举
     */
    public ApiResult(StatusCode statusCode) {
        this.status = statusCode.getCode();
        this.message = statusCode.getMessage();
    }

    /**
     * 根据状态码和自定义提示信息构建默认ApiResult对象
     * 
     * @param statusCode 状态码枚举
     * @param message    自定义提示信息，会覆盖状态码对象中的默认提示信息
     */
    public ApiResult(StatusCode statusCode, String message) {
        this(statusCode);
        if (message != null) {
            this.message = message;
        }
    }

    /**
     * 根据状态码、自定义提示信息、内部详细信息构建默认ApiResult对象
     * 
     * @param statusCode 状态码枚举
     * @param message    自定义提示信息，会覆盖状态码对象中的默认提示信息
     * @param innerMsg   内部详细信息（一般为详细错误信息）
     */
    public ApiResult(StatusCode statusCode, String message, String innerMsg) {
        this(statusCode, message);
        this.innerMsg = innerMsg;
    }

    // -----------带返回数据构造方法------------

    /**
     * <p>
     * 根据业务数据构建默认ApiResult对象。
     * </p>
     * <p>
     * 状态码默认为ApiStatus.OK
     * </p>
     * 
     * @param data 接口返回的业务数据，格式自由
     */
    public ApiResult(T data) {
        this();
        this.data = data;
    }

    /**
     * <p>
     * 根据业务数据构建默认ApiResult对象；
     * </p>
     * <p>
     * 状态码默认为ApiStatus.OK
     * </p>
     *
     * @param data    接口返回的业务数据，格式自由
     * @param message 自定义提示信息，会覆盖状态码对象中的默认提示信息
     */
    public ApiResult(T data, String message) {
        this(data);
        if (message != null) {
            this.message = message;
        }
    }

    /**
     * <p>
     * 根据状态码和业务数据构建ApiResult对象；
     * </p>
     * <br/>
     * 可使用
     * </p>
     * 
     * @param data       接口返回的业务数据，格式自由；
     * @param statusCode 状态码枚举
     */
    public ApiResult(T data, StatusCode statusCode) {
        this(statusCode);
        this.data = data;
    }

    /**
     * <p>
     * 根据状态码和业务数据构建ApiResult对象；
     * </p>
     * 
     * @param data       接口返回的业务数据，格式自由；
     * @param statusCode 状态码枚举
     * @param message    自定义提示信息，会覆盖状态码对象中的默认提示信息
     */
    public ApiResult(T data, StatusCode statusCode, String message) {
        this(statusCode, message);
        this.data = data;
    }

    /**
     * <p>
     * 根据状态码和业务数据构建ApiResult对象；
     * </p>
     * 
     * @param data       接口返回的业务数据，格式自由；
     * @param statusCode 状态码枚举
     * @param message    自定义提示信息，会覆盖状态码对象中的默认提示信息
     * @param innerMsg   内部详细信息（一般为详细错误信息）
     */
    public ApiResult(T data, StatusCode statusCode, String message, String innerMsg) {
        this(statusCode, message, innerMsg);
        this.data = data;
    }

    // -------- getter and setter------------------

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getInnerMsg() {
        return innerMsg;
    }

    public void setInnerMsg(String innerMsg) {
        this.innerMsg = innerMsg;
    }
    
    public boolean isSuccess(){
        return this.status.compareTo(0) >= 0;
    }

    @Override
    public String toString() {
        return "status=" + status + ", message=" + message + ", data=" + data + ", innerMsg=" + innerMsg;
    }

    public static void main(String[] args) {
        System.out.println(new ApiResult<String>().toString());
        System.out.println(new ApiResult<String>(StatusEnum.ERROR).toString());
        System.out.println(new ApiResult<String>(StatusEnum.ERROR, "哎呀，出现异常了").toString());
        System.out.println(new ApiResult<String>(StatusEnum.ERROR, "哎呀，出现异常了", "我是详细异常描述").toString());
        System.out.println(new ApiResult<>("返回一些数据").toString());
        System.out.println(new ApiResult<>("返回一些数据", "查询成功").toString());
        System.out.println(new ApiResult<>("返回一些数据", StatusEnum.ERROR).toString());
        System.out.println(new ApiResult<>("返回一些数据", StatusEnum.ERROR, "失败了，但是还是返回了数据").toString());
        System.out.println(new ApiResult<>("返回一些数据", StatusEnum.ERROR, "失败了，但是还是返回了数据", "我是详细异常描述").toString());
    }

}
