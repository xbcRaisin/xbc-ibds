package com.xbc.ibds.spi.dto;

import java.util.List;

/**
 * @Description: 角色
 * @Author: weizeng@myhexin.com
 * @Date: 2019/12/20 14:23
 * @Version: 1.0
 */
public class Role {
    private String id;

    private List<Permission> permissions;

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
