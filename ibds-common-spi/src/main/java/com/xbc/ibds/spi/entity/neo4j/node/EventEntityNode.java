package com.xbc.ibds.spi.entity.neo4j.node;

import org.neo4j.ogm.annotation.NodeEntity;

/**
 * 描述：事件实体
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@NodeEntity(label = "事件实体")
public class EventEntityNode extends BaseNode{


    private static final long serialVersionUID = -5041877099702968949L;
}
