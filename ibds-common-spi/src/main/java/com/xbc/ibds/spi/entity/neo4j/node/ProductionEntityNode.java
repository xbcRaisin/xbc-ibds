package com.xbc.ibds.spi.entity.neo4j.node;

import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.springframework.util.StringUtils;

/**
 * 描述：产品实体
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@NodeEntity(label = "产品实体")
public class ProductionEntityNode extends BaseNode {

    private static final long serialVersionUID = -8909378714667095601L;

    @Id
    @Property("name")
    private String name;

    @Property("firstLevel")
    private String firstLevel;

    @Property("secondLevel")
    private String secondLevel;

    @Property("thirdLevel")
    private String thirdLevel;

    public ProductionEntityNode() {
    }

    public ProductionEntityNode(String name) {
        super();
    }

    public ProductionEntityNode(String name, String firstLevel, String secondLevel, String thirdLevel) {
        this.name = name;
        this.firstLevel = firstLevel;
        this.secondLevel = secondLevel;
        this.thirdLevel = thirdLevel;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFirstLevel() {
        return firstLevel;
    }

    public void setFirstLevel(String firstLevel) {
        this.firstLevel = firstLevel;
    }

    public String getSecondLevel() {
        return secondLevel;
    }

    public void setSecondLevel(String secondLevel) {
        this.secondLevel = secondLevel;
    }

    public String getThirdLevel() {
        return thirdLevel;
    }

    public void setThirdLevel(String thirdLevel) {
        this.thirdLevel = thirdLevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        //地址相同
        if(this == obj){
            return true;
        }
        //非空性：对于任意非空引用x，x.equals(null)应该返回false。
        if(obj == null){
            return false;
        }

        if(obj instanceof ProductionEntityNode){
            ProductionEntityNode other = (ProductionEntityNode) obj;
            //需要比较的字段相等，则这两个对象相等
            if(equalsStr(this.name, other.name)
                    && equalsStr(this.firstLevel, other.firstLevel)){
                return true;
            }
        }
        return false;
    }

    private boolean equalsStr(String str1, String str2){
        if(StringUtils.isEmpty(str1) && StringUtils.isEmpty(str2)){
            return true;
        }
        if(!StringUtils.isEmpty(str1) && str1.equals(str2)){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (name == null ? 0 : name.hashCode());
        result = 31 * result + (firstLevel == null ? 0 : firstLevel.hashCode());
        return result;
    }
}
