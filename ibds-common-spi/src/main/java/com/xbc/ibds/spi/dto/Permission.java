package com.xbc.ibds.spi.dto;

/**
 * @Description: 权限
 * @Author: weizeng@myhexin.com
 * @Date: 2019/12/20 14:27
 * @Version: 1.0
 */
public class Permission {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
