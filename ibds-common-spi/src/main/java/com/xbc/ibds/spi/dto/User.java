package com.xbc.ibds.spi.dto;

import java.util.List;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 用户实体
 * @Date: 2019/12/18 11:03
 * @Version: 1.0
 */
public class User {
    private Integer id;
    private String username;

    private List<Role> Roles;

    public List<Role> getRoles() {
        return Roles;
    }

    public void setRoles(List<Role> roles) {
        Roles = roles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
