package com.xbc.ibds.spi.entity.neo4j.relation;

import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import org.neo4j.ogm.annotation.RelationshipEntity;

import java.io.Serializable;

/**
 * 描述：运维材料关系
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@RelationshipEntity(type = "运维材料")
public class OperationsMaterialRelation extends BaseRelation<ProductionEntityNode, ProductionEntityNode> implements Serializable {

    private static final long serialVersionUID = -3106244051979658827L;

    public OperationsMaterialRelation(String descInfo, ProductionEntityNode startNode, ProductionEntityNode endNode) {
        super(descInfo, startNode, endNode);
    }

    public OperationsMaterialRelation() {
        super();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
