package com.xbc.ibds.spi.entity.neo4j;

import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import com.xbc.ibds.spi.entity.neo4j.relation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/12
 */
public class SaveEntity implements Serializable {

    private static final long serialVersionUID = -2518783104683252964L;

    private List<ProductionEntityNode> nodeList = new ArrayList<>();

    private List<RawMaterialRelation> rawMaterialRelations = new ArrayList<>();

    private List<ProductionDeviceRelation> productionDeviceRelations = new ArrayList<>();

    private List<OperationsDeviceRelation> operationsDeviceRelations = new ArrayList<>();

    private List<OperationsMaterialRelation> operationsMaterialRelations = new ArrayList<>();

    private List<DistributionChannelRelation> distributionChannelRelations = new ArrayList<>();

    private List<TechnicalServiceRelation> technicalServiceRelations = new ArrayList<>();

    public List<ProductionEntityNode> getNodeList() {
        return nodeList;
    }

    public void setNodeList(List<ProductionEntityNode> nodeList) {
        this.nodeList = nodeList;
    }

    public List<RawMaterialRelation> getRawMaterialRelations() {
        return rawMaterialRelations;
    }

    public void setRawMaterialRelations(List<RawMaterialRelation> rawMaterialRelations) {
        this.rawMaterialRelations = rawMaterialRelations;
    }

    public List<ProductionDeviceRelation> getProductionDeviceRelations() {
        return productionDeviceRelations;
    }

    public void setProductionDeviceRelations(List<ProductionDeviceRelation> productionDeviceRelations) {
        this.productionDeviceRelations = productionDeviceRelations;
    }

    public List<OperationsDeviceRelation> getOperationsDeviceRelations() {
        return operationsDeviceRelations;
    }

    public void setOperationsDeviceRelations(List<OperationsDeviceRelation> operationsDeviceRelations) {
        this.operationsDeviceRelations = operationsDeviceRelations;
    }

    public List<OperationsMaterialRelation> getOperationsMaterialRelations() {
        return operationsMaterialRelations;
    }

    public void setOperationsMaterialRelations(List<OperationsMaterialRelation> operationsMaterialRelations) {
        this.operationsMaterialRelations = operationsMaterialRelations;
    }

    public List<DistributionChannelRelation> getDistributionChannelRelations() {
        return distributionChannelRelations;
    }

    public void setDistributionChannelRelations(List<DistributionChannelRelation> distributionChannelRelations) {
        this.distributionChannelRelations = distributionChannelRelations;
    }

    public List<TechnicalServiceRelation> getTechnicalServiceRelations() {
        return technicalServiceRelations;
    }

    public void setTechnicalServiceRelations(List<TechnicalServiceRelation> technicalServiceRelations) {
        this.technicalServiceRelations = technicalServiceRelations;
    }
}
