package com.xbc.ibds.spi.entity.neo4j.relation;

import com.xbc.ibds.spi.entity.neo4j.node.BaseNode;
import org.neo4j.ogm.annotation.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述：
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@RelationshipEntity(type = "KNOWLEDGE_GRAPH_RELATION")
public class BaseRelation<S extends BaseNode,E extends BaseNode> implements Serializable {
    private static final long serialVersionUID = -5324364947829437087L;

    @Id
    @GeneratedValue
    private Long id;

    @Property("cTime")
    private Date cTime;

    @Property("mTime")
    private Date mTime;

    @Property("rTime")
    private Date rTime;

    @Property("descInfo")
    private String descInfo;

    @StartNode
    private S startNode;

    @EndNode
    private E endNode;

    public BaseRelation() {

    }

    public BaseRelation(String descInfo, S startNode, E endNode) {
        Date date = new Date();
        this.cTime = date;
        this.mTime = date;
        this.rTime = date;
        this.descInfo = descInfo;
        this.startNode = startNode;
        this.endNode = endNode;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getcTime() {
        return cTime;
    }

    public void setcTime(Date cTime) {
        this.cTime = cTime;
    }

    public Date getmTime() {
        return mTime;
    }

    public void setmTime(Date mTime) {
        this.mTime = mTime;
    }

    public Date getrTime() {
        return rTime;
    }

    public void setrTime(Date rTime) {
        this.rTime = rTime;
    }

    public String getDescInfo() {
        return descInfo;
    }

    public void setDescInfo(String descInfo) {
        this.descInfo = descInfo;
    }

    public S getStartNode() {
        return startNode;
    }

    public void setStartNode(S startNode) {
        this.startNode = startNode;
    }

    public E getEndNode() {
        return endNode;
    }

    public void setEndNode(E endNode) {
        this.endNode = endNode;
    }
}
