package com.xbc.ibds.spi.entity.neo4j.node;

import org.neo4j.ogm.annotation.NodeEntity;

/**
 * 描述：公司实体
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@NodeEntity(label = "公司实体")
public class CompanyEntityNode extends BaseNode {


    private static final long serialVersionUID = 7619294205825927408L;
}
