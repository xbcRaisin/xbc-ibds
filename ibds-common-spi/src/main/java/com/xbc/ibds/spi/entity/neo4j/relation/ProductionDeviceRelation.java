package com.xbc.ibds.spi.entity.neo4j.relation;

import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import org.neo4j.ogm.annotation.RelationshipEntity;

import java.io.Serializable;

/**
 * 描述：生产设备关系
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@RelationshipEntity(type = "生产设备")
public class ProductionDeviceRelation extends BaseRelation<ProductionEntityNode, ProductionEntityNode> implements Serializable {

    private static final long serialVersionUID = 3901573803212507288L;

    public ProductionDeviceRelation(String descInfo, ProductionEntityNode startNode, ProductionEntityNode endNode) {
        super(descInfo, startNode, endNode);
    }

    public ProductionDeviceRelation() {
        super();
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
