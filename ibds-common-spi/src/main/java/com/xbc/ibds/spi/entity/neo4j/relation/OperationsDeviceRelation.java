package com.xbc.ibds.spi.entity.neo4j.relation;

import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import org.neo4j.ogm.annotation.RelationshipEntity;

import java.io.Serializable;

/**
 * 描述：运维设备关系
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@RelationshipEntity(type = "运维设备")
public class OperationsDeviceRelation extends BaseRelation<ProductionEntityNode,ProductionEntityNode> implements Serializable {

    private static final long serialVersionUID = -1966915203621295699L;

    public OperationsDeviceRelation(String descInfo, ProductionEntityNode startNode, ProductionEntityNode endNode) {
        super(descInfo, startNode, endNode);
    }
    public OperationsDeviceRelation() {
        super();
    }
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
