package com.xbc.ibds.spi.entity.neo4j.node;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述：
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@NodeEntity(label = "KNOWLEDGE_GRAPH_NODE")
public class BaseNode implements Serializable {

    private static final long serialVersionUID = -3445203951428080050L;

    @Property("cTime")
    private Date cTime;

    @Property("mTime")
    private Date mTime;

    @Property("rTime")
    private Date rTime;

    public BaseNode() {
        Date date = new Date();
        this.cTime = date;
        this.mTime = date;
        this.rTime = date;
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getcTime() {
        return cTime;
    }

    public void setcTime(Date cTime) {
        this.cTime = cTime;
    }

    public Date getmTime() {
        return mTime;
    }

    public void setmTime(Date mTime) {
        this.mTime = mTime;
    }

    public Date getrTime() {
        return rTime;
    }

    public void setrTime(Date rTime) {
        this.rTime = rTime;
    }

}
