package com.xbc.ibds.spi.entity.neo4j.relation;

import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import org.neo4j.ogm.annotation.RelationshipEntity;

import java.io.Serializable;

/**
 * 描述：原材料关系
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@RelationshipEntity(type = "原材料")
public class RawMaterialRelation extends BaseRelation<ProductionEntityNode, ProductionEntityNode> implements Serializable {

    private static final long serialVersionUID = -4145688082009567907L;

    public RawMaterialRelation(String descInfo, ProductionEntityNode startNode, ProductionEntityNode endNode) {
        super(descInfo, startNode, endNode);
    }

    public RawMaterialRelation() {
        super();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
