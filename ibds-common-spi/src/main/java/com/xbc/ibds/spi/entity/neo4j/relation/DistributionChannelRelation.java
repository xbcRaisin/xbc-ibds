package com.xbc.ibds.spi.entity.neo4j.relation;

import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import org.neo4j.ogm.annotation.RelationshipEntity;

import java.io.Serializable;

/**
 * 描述：销售渠道关系
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/11
 */
@RelationshipEntity(type = "销售渠道")
public class DistributionChannelRelation extends BaseRelation<ProductionEntityNode, ProductionEntityNode> implements Serializable {

    private static final long serialVersionUID = -7046852622016498749L;

    public DistributionChannelRelation(String descInfo, ProductionEntityNode startNode, ProductionEntityNode endNode) {
        super(descInfo, startNode, endNode);
    }

    public DistributionChannelRelation() {
        super();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
