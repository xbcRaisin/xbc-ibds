#!/bin/bash
echo '>>clean xbc-ibds file'
rm -rf xbc-ibds
echo '>>clone from gitee'
git clone git@gitee.com:xbcRaisin/xbc-ibds.git
tag_name=1.0.0-$(date +%Y%m%d%H%M)
echo tag_name: ${tag_name}
cd /data/docker-project/xbc-ibds/shell/xbc-ibds/
chmod 755 *
echo '>>rebuild'
sh ./builder_image.sh rebuild ${tag_name}

sed -i 's/{image-tag}/'${tag_name}'/g' docker-compose.yml
echo '>>cat docker-compose.yml'
cat docker-compose.yml
echo -e "----------------------docker-compose----------------------------"
docker-compose up -d
