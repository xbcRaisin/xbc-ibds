#!/bin/bash

cd `dirname $0`

LOG_DIR=/logs
SPRING_PROFILES_ACTIVE=${THS_TIER}

JAVA_OPTS="-server -Dfile.encoding=utf-8 -XX:MaxDirectMemorySize=128m -XX:MaxMetaspaceSize=128m -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:${LOG_DIR}/gc.log -XX:+HeapDumpOnOutOfMemoryError -XX:ErrorFile=${LOG_DIR}/java_error_%p.log -XX:HeapDumpPath=${LOG_DIR}/java_error.hprof"

if [ "x${JAVA_MEM_OPTS}" = "x" ]; then
    JAVA_MEM_OPTS="-Xms$((${THS_MEM_REQUEST}*30/100))m -Xmx$((${THS_MEM_LIMIT}*60/100))m -Xss512k -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"
fi

JAVA_JMX_OPTS=""
if [ "x${JMX_PORT}" != "x" ]; then
    JAVA_JMX_OPTS="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=${JMX_PORT} -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false  "
fi

#cat /data/hosts >> /etc/hosts
#java ${JAVA_OPTS} ${JAVA_MEM_OPTS} ${JAVA_JMX_OPTS} -jar main.jar --spring.profiles.active=${THS_TIER}

echo "exec java ${JAVA_OPTS} ${JAVA_MEM_OPTS} ${JAVA_JMX_OPTS} -jar main.jar --spring.profiles.active=${SPRING_PROFILES_ACTIVE}"
java ${JAVA_OPTS} ${JAVA_MEM_OPTS} ${JAVA_JMX_OPTS} -jar main.jar --spring.profiles.active=${SPRING_PROFILES_ACTIVE}
