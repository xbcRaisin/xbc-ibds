#!/usr/bin/env bash

group=xiaobaicai/pupil
app_name=xbc-ibds
tag=$(date "+%Y%m%d%H%M")

if [ "x$2" != "x" ]; then
  tag=$2
fi
#hub=hub.docker.com

image_name="$group:$app_name-$tag"
echo image name:  $image_name

function push_registry(){
echo -e "\nexec push images into registry ....."
#docker tag ${image_name} xiaobaicai/pupil:${image_name}
#docker login -u "xiaobaicai" -p "Wei1126shazi"
#docker push $image_name
}

function build_image(){
echo $image_name && \
docker build -t ${image_name} .
}

function clean(){
echo -e "\nexec clean ....."
docker rm -f ${app_name}
#docker rmi -f ${image_name}
docker image prune -af
}

function rebuild(){
echo "---------------------rebuild---------------------"
clean
build_image
push_registry
}

if [ "x$1" == "x" ]; then
  echo "---------------------------------------------------------------------------------"
elif [ "$1" == "build_image" ]; then
  build_image
elif [ "$1" == "push_registry" ]; then
  push_registry
elif [ "$1" == "clean" ]; then
  clean
elif [ "$1" == "rebuild" ]; then
  rebuild
fi