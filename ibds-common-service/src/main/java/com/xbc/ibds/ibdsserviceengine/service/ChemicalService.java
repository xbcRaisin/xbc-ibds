package com.xbc.ibds.ibdsserviceengine.service;

import com.xbc.ibds.ibdsserviceengine.dto.ChemicalExcelModel;
import com.xbc.ibds.spi.entity.neo4j.SaveEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author: weizeng@myhexin.com
 * @Description:
 * @Date: 2019/12/18 13:32
 * @Version: 1.0
 */
public interface ChemicalService {

    /**
     * @param file
     * @param filePath
     * @return
     */
    String parseExcelData(MultipartFile file, String filePath, String industryName);

    String clearChemical();
}
