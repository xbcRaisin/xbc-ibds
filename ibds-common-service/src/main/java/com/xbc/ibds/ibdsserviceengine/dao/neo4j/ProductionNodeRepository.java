package com.xbc.ibds.ibdsserviceengine.dao.neo4j;

import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import org.springframework.data.neo4j.repository.Neo4jRepository;

/**
 * 描述：
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/5
 */
public interface ProductionNodeRepository extends Neo4jRepository<ProductionEntityNode, Long> {

    /*Person findByfirstName(@Param("firstName") String firstName);

    Collection<Person> findByfirstNameLike(@Param("firstName") String firstName);

    @Query(value = "MATCH (movie:Movie {title:{0}})<-[:ACTED_IN]-(p:Person) RETURN p",
            countQuery= "MATCH (movie:Movie {title:{0}})<-[:ACTED_IN]-(p:Person) RETURN count(p)")
    Page<Person> getActorsThatActInAmoviesFromTitle(String movieTitle, Pageable pageable);*/
}
