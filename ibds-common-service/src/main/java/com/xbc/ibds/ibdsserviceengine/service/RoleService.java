package com.xbc.ibds.ibdsserviceengine.service;

import com.xbc.ibds.spi.dto.Role;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 角色接口层
 * @Date: 2019/12/18 13:32
 * @Version: 1.0
 */
public interface RoleService {

    Integer count();

    Role getRoleById(String id);
}
