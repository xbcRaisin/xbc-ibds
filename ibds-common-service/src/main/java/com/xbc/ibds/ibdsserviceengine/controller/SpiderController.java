package com.xbc.ibds.ibdsserviceengine.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：
 *
 * @author weizeng
 * @date 2019/11/27 14:47
 */
@RestController
@RequestMapping("/spider")
public class SpiderController {
    Logger logger = LoggerFactory.getLogger("SpiderController.class");

    @RequestMapping("/getImage")
    public String getImage(String url) {
        return url;
    }
}
