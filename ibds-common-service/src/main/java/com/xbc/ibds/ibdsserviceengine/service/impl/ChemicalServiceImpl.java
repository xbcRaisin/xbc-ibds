package com.xbc.ibds.ibdsserviceengine.service.impl;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.xbc.ibds.ibdsserviceengine.dto.ChemicalExcelModel;
import com.xbc.ibds.ibdsserviceengine.service.ChemicalSaveService;
import com.xbc.ibds.ibdsserviceengine.service.ChemicalService;
import com.xbc.ibds.ibdsserviceengine.util.ChemicalDataListener;
import com.xbc.ibds.spi.entity.neo4j.SaveEntity;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 用户服务实现层
 * @Date: 2019/12/18 13:33
 * @Version: 1.0
 */
@Service
public class ChemicalServiceImpl implements ChemicalService {
    private static final Logger logger = LoggerFactory.getLogger(ChemicalServiceImpl.class);

    @Autowired
    private ChemicalSaveService chemicalSaveService;

    @Override
    public String parseExcelData(MultipartFile file, String filePath, String industryName) {
        InputStream inputStream = null;
        try {
            if (file != null) {
                inputStream = file.getInputStream();
            } else if (StringUtils.isNotEmpty(filePath)) {
                inputStream = new FileInputStream(filePath);
            }
            //headLineMun 1 从第二行开始读
            Sheet sheet = new Sheet(1, 3, ChemicalExcelModel.class);
            ChemicalDataListener listener = new ChemicalDataListener(this.chemicalSaveService, industryName);
            EasyExcelFactory.readBySax(inputStream, sheet, listener);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = "成功！";
        return result;
    }

    @Override
    public String clearChemical() {

        return chemicalSaveService.clearChemical();
    }
}
