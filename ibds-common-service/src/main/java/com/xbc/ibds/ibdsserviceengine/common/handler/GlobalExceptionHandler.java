package com.xbc.ibds.ibdsserviceengine.common.handler;

import com.xbc.ibds.core.exception.BizException;
import com.xbc.ibds.core.status.enums.StatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述：统一异常处理
 *
 * @author weizeng@myhexin.com
 * @date 2019/12/25 15:59
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Autowired
    private RequestMappingHandlerMapping handlerMapping;

    @ExceptionHandler(value = Throwable.class)
    public ModelAndView handlerError(HttpServletRequest request, Throwable throwable) {
        if(logger.isDebugEnabled()){
            logger.error("GlobalExceptionHandler>>>>",throwable);
        }
        try{
            HandlerExecutionChain chain = handlerMapping.getHandler(request);
            Object handler = chain.getHandler();
            if(handler != null && handler instanceof HandlerMethod){
                HandlerMethod handlerMethod = (HandlerMethod) handler;
                Method method = handlerMethod.getMethod();
                ResponseBody responseBodyAnn = AnnotationUtils.findAnnotation(method, ResponseBody.class);
                RestController restControllerAnn = AnnotationUtils.findAnnotation(method.getDeclaringClass(), RestController.class);
                if(responseBodyAnn != null || restControllerAnn != null){
                    return handlerAjaxError(throwable);
                }
            }

        }catch (Exception e){
            logger.error("[handlerError]",e);

        }
        return handlerViewError("error/error",throwable.getMessage(),throwable);
    }

    protected ModelAndView handlerViewError(String viewName, String errorStack, Throwable throwable){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", errorStack);
        if(throwable instanceof MethodArgumentTypeMismatchException){
            modelAndView.addObject("message", "请求参数异常");
        } else if(throwable instanceof NumberFormatException){
            modelAndView.addObject("message", "内部数值转换错误");
            logger.error("[handlerViewError]",throwable);
        } /*else if(throwable instanceof AuthorizationException){
            modelAndView.addObject("message", "没有该页面权限");
        }*/ else {
            modelAndView.addObject("message", "系统出现了错误");
            logger.error("[handlerViewError]",throwable);
        }
        return modelAndView;
    }

    protected ModelAndView handlerAjaxError(Throwable throwable){
        ModelAndView modelAndView = new ModelAndView();
        Map<String,Object> attributes = new HashMap<>(5);
        if (throwable instanceof BizException) {
            BizException ex = (BizException) throwable;
            attributes.put("status",ex.getCode());
            attributes.put("message",ex.getMessage());
            //attributes.put("innerMsg",ex.getDetailMessage());
            logger.info("[handlerAjaxError]handler BizException:code={},message={}",ex.getCode(),ex.getMessage());
        } /*else if(throwable instanceof AuthorizationException){
            attributes.put("status", StatusEnum.NO_PERMISSION.getCode());
            attributes.put("message",StatusEnum.NO_PERMISSION.getMessage());
            //attributes.put("innerMsg",throwable.getMessage());
        } */else {
            attributes.put("status",StatusEnum.SYS_ERROR.getCode());
            attributes.put("message",StatusEnum.SYS_ERROR.getMessage());
           // attributes.put("innerMsg",throwable.getMessage());
            logger.error("[handlerAjaxError]",throwable);
        }
        attributes.put("innerMsg","system error");
        attributes.put("data",null);
        MappingJackson2JsonView view = new MappingJackson2JsonView();
        view.setAttributesMap(attributes);
        modelAndView.setView(view);
        return modelAndView;
    }
}
