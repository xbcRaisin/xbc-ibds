package com.xbc.ibds.ibdsserviceengine.controller;

import com.xbc.ibds.core.dto.ApiResult;
import com.xbc.ibds.core.status.enums.StatusEnum;
import com.xbc.ibds.core.util.Assert;
import com.xbc.ibds.core.util.StringUtils;
import com.xbc.ibds.ibdsserviceengine.service.ChemicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 描述：
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/5
 */

@RestController
@RequestMapping("/chemical")
public class ChemicalController {

    @Autowired
    private ChemicalService chemicalService;

    /**
     * excel 解析解析异步插入neo4j
     *
     * @param file
     * @param filePath
     * @return
     */
    @RequestMapping(value = "/parseExcel", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult<String> parseExcelData(MultipartFile file, String filePath, String industryName) {
        Assert.isTrue(!(file == null && StringUtils.isEmpty(filePath)), StatusEnum.MISSING_PARAM, "解析文件file 或者filePath不能都为空");
        return new ApiResult(chemicalService.parseExcelData(file, filePath, industryName));
    }

    /**
     * @return
     */
    @RequestMapping(value = "/clearChemical", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult<String> clearChemical() {
        return new ApiResult(chemicalService.clearChemical());
    }
}