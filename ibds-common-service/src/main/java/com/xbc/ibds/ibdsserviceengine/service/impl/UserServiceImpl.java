package com.xbc.ibds.ibdsserviceengine.service.impl;

import com.xbc.ibds.ibdsserviceengine.dao.UserDao;
import com.xbc.ibds.ibdsserviceengine.service.UserService;
import com.xbc.ibds.spi.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 用户服务实现层
 * @Date: 2019/12/18 13:33
 * @Version: 1.0
 */
@Service("UserServiceImpl")
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public Integer count() {
        return userDao.count();
    }

    @Override
    public User getUserById(String username) {
        return null;
    }
}
