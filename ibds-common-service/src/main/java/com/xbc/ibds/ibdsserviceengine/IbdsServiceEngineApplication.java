package com.xbc.ibds.ibdsserviceengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@org.springframework.context.annotation.Configuration
public class IbdsServiceEngineApplication {

    public static void main(String[] args) {
        SpringApplication.run(IbdsServiceEngineApplication.class, args);
    }

}
