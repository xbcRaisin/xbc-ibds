package com.xbc.ibds.ibdsserviceengine.service.impl;

import com.xbc.ibds.ibdsserviceengine.dao.UserDao;
import com.xbc.ibds.ibdsserviceengine.service.RoleService;
import com.xbc.ibds.spi.dto.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 用户服务实现层
 * @Date: 2019/12/18 13:33
 * @Version: 1.0
 */
@Service("RoleServiceImpl")
public class RoleServiceImpl implements RoleService {

    @Autowired
    UserDao userDao;

    @Override
    public Integer count() {
        return userDao.count();
    }

    @Override
    public Role getRoleById(String id) {
        return null;
    }
}
