package com.xbc.ibds.ibdsserviceengine.service;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 资源接口层
 * @Date: 2019/12/18 13:32
 * @Version: 1.0
 */
public interface ResourceService {

    Integer count();
}
