package com.xbc.ibds.ibdsserviceengine.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

/**
 * 描述：
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/6
 */
@Data
public class ChemicalExcelModel extends BaseRowModel {

    @ExcelProperty(value = "产品词申万一级行业", index = 0)
    private String productFirstLevel;

    @ExcelProperty(value = "产品词申万二级行业", index = 1)
    private String productSecondLevel;

    @ExcelProperty(value = "产品词申万三级行业", index = 2)
    private String productThirdLevel;

    @ExcelProperty(value = "产品名称", index = 3)
    private String productName;

    @ExcelProperty(value = "上下游关系类型", index = 4)
    private String relationDirection;

    @ExcelProperty(value = "关系类型", index = 5)
    private String relationType;

    @ExcelProperty(value = "上下游产品名称", index = 6)
    private String otherProductName;

    @ExcelProperty(value = "申万一级行业", index = 7)
    private String firstLevel;

    @ExcelProperty(value = "申万二级行业", index = 8)
    private String secondLevel;

    @ExcelProperty(value = "申万三级行业", index = 9)
    private String thirdLevel;

    public ChemicalExcelModel() {
    }

    public String getProductFirstLevel() {
        return productFirstLevel;
    }

    public void setProductFirstLevel(String productFirstLevel) {
        this.productFirstLevel = productFirstLevel;
    }

    public String getProductSecondLevel() {
        return productSecondLevel;
    }

    public void setProductSecondLevel(String productSecondLevel) {
        this.productSecondLevel = productSecondLevel;
    }

    public String getProductThirdLevel() {
        return productThirdLevel;
    }

    public void setProductThirdLevel(String productThirdLevel) {
        this.productThirdLevel = productThirdLevel;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getRelationDirection() {
        return relationDirection;
    }

    public void setRelationDirection(String relationDirection) {
        this.relationDirection = relationDirection;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public String getOtherProductName() {
        return otherProductName;
    }

    public void setOtherProductName(String otherProductName) {
        this.otherProductName = otherProductName;
    }

    public String getFirstLevel() {
        return firstLevel;
    }

    public void setFirstLevel(String firstLevel) {
        this.firstLevel = firstLevel;
    }

    public String getSecondLevel() {
        return secondLevel;
    }

    public void setSecondLevel(String secondLevel) {
        this.secondLevel = secondLevel;
    }

    public String getThirdLevel() {
        return thirdLevel;
    }

    public void setThirdLevel(String thirdLevel) {
        this.thirdLevel = thirdLevel;
    }
}
