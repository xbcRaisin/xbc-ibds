package com.xbc.ibds.ibdsserviceengine.common.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 描述：线程池调用配置类
 */
@Configuration
public class ThreadPoolConfiguration {
    /**
     * 核心线程数
     */
    private static final int CORE_POOL_SIZE = 10;
    /**
     * 最大线程数
     */
    private static final int MAX_POOL_SIZE = 50;
    /**
     * 队列大小
     */
    private static final int QUEUE_CAPACITY = 50;
    /**
     * 链接时间
     */
    private static final int KEEP_ALIVE_SECONDS = 300;

    @Bean(name = "concurrentTaskExecutor")
    @Qualifier("concurrentTaskExecutor")
    public ThreadPoolTaskExecutor concurrentTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(CORE_POOL_SIZE);
        executor.setMaxPoolSize(MAX_POOL_SIZE);
        executor.setQueueCapacity(QUEUE_CAPACITY);
        executor.setKeepAliveSeconds(KEEP_ALIVE_SECONDS);
        // 无线程可用的处理策略：主线程直接执行该任务，执行完之后尝试添加下一个任务到线程池中
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }
}
