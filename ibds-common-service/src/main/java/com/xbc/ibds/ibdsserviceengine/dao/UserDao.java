package com.xbc.ibds.ibdsserviceengine.dao;

import org.springframework.stereotype.Repository;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 用户查询
 * @Date: 2019/12/18 10:54
 * @Version: 1.0
 */
@Repository("userDao")
public interface UserDao {

    Integer count();
}
