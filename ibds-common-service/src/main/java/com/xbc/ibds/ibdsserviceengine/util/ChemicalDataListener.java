package com.xbc.ibds.ibdsserviceengine.util;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.xbc.ibds.core.util.StringUtils;
import com.xbc.ibds.ibdsserviceengine.dto.ChemicalExcelModel;
import com.xbc.ibds.ibdsserviceengine.service.ChemicalSaveService;
import com.xbc.ibds.spi.entity.neo4j.SaveEntity;
import com.xbc.ibds.spi.entity.neo4j.node.BaseNode;
import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import com.xbc.ibds.spi.entity.neo4j.relation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述：有个很重要的点 ChemicalDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
 *
 * @author weizeng@myhexin.com
 * @date 2020/6/6
 */
public class ChemicalDataListener extends AnalysisEventListener<ChemicalExcelModel> {

    private static final Logger logger = LoggerFactory.getLogger(ChemicalDataListener.class);

    private static final int BATCH_COUNT = 1000;

    List<ChemicalExcelModel> chemicalExcelModelList = new ArrayList<>();

    private int count = 0;
    private int total = 0;

    private String industryName;

    private ChemicalSaveService chemicalSaveService;

    private Map<String, BaseNode> nodeMap = new HashMap<>();

    public ChemicalDataListener() {
    }

    public ChemicalDataListener(ChemicalSaveService chemicalSaveService, String industryName) {
        this.chemicalSaveService = chemicalSaveService;
        this.industryName = industryName;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data
     * @param context
     */
    @Override
    public void invoke(ChemicalExcelModel data, AnalysisContext context) {
        total++;
        logger.info("industryName:{},解析到一条数据:{}", data.getProductFirstLevel(), data.getOtherProductName());
        if ("化工".equals(data.getProductFirstLevel())) {
            chemicalExcelModelList.add(data);
            count++;
            // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
            if (chemicalExcelModelList.size() >= BATCH_COUNT) {
                saveChemicalData();
                chemicalExcelModelList.clear();
            }
        }
    }

    /**
     *
     */
    public void saveChemicalData() {
        SaveEntity saveEntity = new SaveEntity();
        logger.info("开始保存saveEntity到neo4j数据库,保存行数：{}", chemicalExcelModelList.size());
        chemicalExcelModelList.forEach(c -> {
            parseEveryDate(c, saveEntity);
        });
        chemicalSaveService.saveAllRepository(saveEntity);
        logger.info("保存saveEntity到neo4j数据库-成功,保存行数：{}", chemicalExcelModelList.size());
        chemicalExcelModelList.clear();
        logger.info("清空缓存数据列表成功chemicalExcelModelList Size：{}", chemicalExcelModelList.size());
        logger.info("已处理 excel数据行数total:{},化工:count:{}", total, count);
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveChemicalData();
        logger.info("所有数据解析完成！");
    }

    /**
     * @param data
     */
    public String parseEveryDate(ChemicalExcelModel data, SaveEntity saveEntity) {
        String pName = data.getProductName();
        String pFirstL = data.getProductFirstLevel();
        String pSecondL = data.getProductSecondLevel();
        String pThirdL = data.getProductThirdLevel();
        String name = data.getOtherProductName();
        String firstL = data.getFirstLevel();
        String secondL = data.getSecondLevel();
        String thirdL = data.getThirdLevel();
        String startKey = pName + "_" + pFirstL + "_" + pSecondL + "_" + pThirdL;
        String EndKey = name + "_" + firstL + "_" + secondL + "_" + thirdL;
        if (StringUtils.isEmpty(pName) || StringUtils.isEmpty(name)) {
            return "";
        }
        ProductionEntityNode startNode = new ProductionEntityNode(pName, pFirstL, pSecondL, pThirdL);
        ProductionEntityNode endNode = new ProductionEntityNode(name, firstL, secondL, thirdL);
        //保存节点
        if (!nodeMap.containsKey(startKey)) {
            nodeMap.put(startKey, startNode);
            saveEntity.getNodeList().add(startNode);
        }
        if (!nodeMap.containsKey(EndKey)) {
            nodeMap.put(EndKey, endNode);
            saveEntity.getNodeList().add(endNode);
        }
        //处理关系
        String relationCode = data.getRelationDirection();
        if (StringUtils.isEmpty(relationCode)) {
            return "";
        }
        if (Integer.parseInt(relationCode) < 0) {
            ProductionEntityNode nodeItem = startNode;
            startNode = endNode;
            endNode = nodeItem;
        }
        String relationType = data.getRelationType();
        if ("生产设备".equals(relationType)) {
            ProductionDeviceRelation relation = new ProductionDeviceRelation();
            relation.setDescInfo(relationType);
            relation.setStartNode(startNode);
            relation.setEndNode(endNode);
            saveEntity.getProductionDeviceRelations().add(relation);
        } else if ("运维设备".equals(relationType)) {
            OperationsDeviceRelation relation = new OperationsDeviceRelation();
            relation.setDescInfo(relationType);
            relation.setStartNode(startNode);
            relation.setEndNode(endNode);
            saveEntity.getOperationsDeviceRelations().add(relation);
        } else if ("原材料".equals(relationType)) {
            RawMaterialRelation relation = new RawMaterialRelation();
            relation.setDescInfo(relationType);
            relation.setStartNode(startNode);
            relation.setEndNode(endNode);
            saveEntity.getRawMaterialRelations().add(relation);
        } else if ("运维材料".equals(relationType)) {
            OperationsMaterialRelation relation = new OperationsMaterialRelation();
            relation.setDescInfo(relationType);
            relation.setStartNode(startNode);
            relation.setEndNode(endNode);
            saveEntity.getOperationsMaterialRelations().add(relation);
        } else if ("销售渠道".equals(relationType)) {
            DistributionChannelRelation relation = new DistributionChannelRelation();
            relation.setDescInfo(relationType);
            relation.setStartNode(startNode);
            relation.setEndNode(endNode);
            saveEntity.getDistributionChannelRelations().add(relation);
        } else if ("技术服务".equals(relationType)) {
            TechnicalServiceRelation relation = new TechnicalServiceRelation();
            relation.setDescInfo(relationType);
            relation.setStartNode(startNode);
            relation.setEndNode(endNode);
            saveEntity.getTechnicalServiceRelations().add(relation);
        }
        return "OK";
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }
}
