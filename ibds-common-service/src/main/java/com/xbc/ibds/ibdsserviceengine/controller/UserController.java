package com.xbc.ibds.ibdsserviceengine.controller;

import com.xbc.ibds.ibdsserviceengine.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: weizeng@myhexin.com
 *
 * @Description: 用户管理
 * @Date: 2019/12/18 13:30
 * @Version: 1.0
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    UserService userService;

    /**
     *@描述
     *
     *@参数
     *
     *@返回值
     *
     *@创建人  weizeng
     *
     *@创建时间  2019/12/19
     *
     */
    @RequestMapping("/count")
    public Integer count(String name,Integer age) {
        log.info("request-count" );
        return 10000;
        //return userService.count();
    }
}
