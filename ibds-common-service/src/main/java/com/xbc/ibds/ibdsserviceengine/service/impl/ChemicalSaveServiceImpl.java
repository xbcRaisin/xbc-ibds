package com.xbc.ibds.ibdsserviceengine.service.impl;

import com.xbc.ibds.core.util.ListUtils;
import com.xbc.ibds.ibdsserviceengine.dao.neo4j.*;
import com.xbc.ibds.ibdsserviceengine.service.ChemicalSaveService;
import com.xbc.ibds.spi.entity.neo4j.SaveEntity;
import com.xbc.ibds.spi.entity.neo4j.node.ProductionEntityNode;
import com.xbc.ibds.spi.entity.neo4j.relation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 用户服务实现层
 * @Date: 2019/12/18 13:33
 * @Version: 1.0
 */
@Service
public class ChemicalSaveServiceImpl implements ChemicalSaveService {

    Logger logger = LoggerFactory.getLogger(ChemicalSaveServiceImpl.class);

    @Autowired
    private ProductionNodeRepository productionNodeRepository;

    @Autowired
    private RawMaterialRelationRepository rawMaterialRelationRepository;

    @Autowired
    private DistributionChannelRelationepository distributionChannelRelationepository;

    @Autowired
    private OperationsDeviceRelationRepository operationsDeviceRelationRepository;

    @Autowired
    private OperationsMaterialRelationRepository operationsMaterialRelationRepository;

    @Autowired
    private ProductionDeviceRelationRepository productionDeviceRelationRepository;

    @Autowired
    private TechnicalServiceRelationRepository technicalServiceRelationRepository;

    @Resource(name = "concurrentTaskExecutor")
    ThreadPoolTaskExecutor taskExecutor;

    @Override
    public String saveAllRepository(SaveEntity saveEntity) {
        long start = System.currentTimeMillis();
        logger.info("saveAllRepository-开始插入-neo4j,NodeList总量:{},getDistributionChannelRelations总量:{}," +
                        "getOperationsDeviceRelations总量:{},getOperationsMaterialRelations总量:{}," +
                        "getProductionDeviceRelations总量:{},getRawMaterialRelations总量:{},getTechnicalServiceRelations总量:{}"
                , saveEntity.getNodeList().size(), saveEntity.getDistributionChannelRelations().size(),
                saveEntity.getOperationsDeviceRelations().size(), saveEntity.getOperationsMaterialRelations().size(),
                saveEntity.getProductionDeviceRelations().size(), saveEntity.getRawMaterialRelations().size(), saveEntity.getTechnicalServiceRelations().size());
        List<ProductionEntityNode> nodeList = saveEntity.getNodeList();
        List<List<ProductionEntityNode>> splitList = ListUtils.splitList(nodeList, 5);
        //并发存节点Node
        splitList.forEach(list -> {
            taskExecutor.execute(() -> {
                productionNodeRepository.saveAll(nodeList);
            });
        });
        //并发存关系
        long relation = System.currentTimeMillis();
        logger.info("并发存储所有节点成功，耗时：{} ms", System.currentTimeMillis() - start);
        /*taskExecutor.execute(() -> {
            rawMaterialRelationRepository.saveAll(saveEntity.getRawMaterialRelations());
        });
        taskExecutor.execute(() -> {
            distributionChannelRelationepository.saveAll(saveEntity.getDistributionChannelRelations());
        });
        taskExecutor.execute(() -> {
            operationsDeviceRelationRepository.saveAll(saveEntity.getOperationsDeviceRelations());
        });
        taskExecutor.execute(() -> {
            operationsMaterialRelationRepository.saveAll(saveEntity.getOperationsMaterialRelations());
        });
        taskExecutor.execute(() -> {
            productionDeviceRelationRepository.saveAll(saveEntity.getProductionDeviceRelations());
        });
        taskExecutor.execute(() -> {
            technicalServiceRelationRepository.saveAll(saveEntity.getTechnicalServiceRelations());
        });*/
        rawMaterialRelationRepository.saveAll(saveEntity.getRawMaterialRelations());
        distributionChannelRelationepository.saveAll(saveEntity.getDistributionChannelRelations());
        operationsDeviceRelationRepository.saveAll(saveEntity.getOperationsDeviceRelations());
        operationsMaterialRelationRepository.saveAll(saveEntity.getOperationsMaterialRelations());
        productionDeviceRelationRepository.saveAll(saveEntity.getProductionDeviceRelations());
        technicalServiceRelationRepository.saveAll(saveEntity.getTechnicalServiceRelations());
        logger.info("neo4j数据插入-所有关系-成功总耗时：{}", System.currentTimeMillis() - relation);
        return "neo4j数据插入成功";
    }

    @Override
    public String clearChemical() {
        productionNodeRepository.deleteAll();
        rawMaterialRelationRepository.deleteAll();
        distributionChannelRelationepository.deleteAll();
        operationsDeviceRelationRepository.deleteAll();
        operationsMaterialRelationRepository.deleteAll();
        productionDeviceRelationRepository.deleteAll();
        technicalServiceRelationRepository.deleteAll();
        return "清空化工产业链neo4j数据成功";
    }
}
