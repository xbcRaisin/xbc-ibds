package com.xbc.ibds.ibdsserviceengine.service;

import com.xbc.ibds.spi.dto.User;

/**
 * @Author: weizeng@myhexin.com
 * @Description: 用户服务接口层
 * @Date: 2019/12/18 13:32
 * @Version: 1.0
 */
public interface UserService {

    Integer count();

    User getUserById(String username);
}
